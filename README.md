# Multiple_Linear_Regression_R

In this R file you can have a look at a multiple linear regression. If you want to run the code on your computer you will have to change your path to load the data series_chrono_data. You will find the following main steps : 

- Calculate matrix of the linear correlations (Pearson) and the matrix of the associated p-values

- Identify the linear correlation the most significant by taking a risk (5%) and interpret

- What are the variables highly correlated ?

- Create multiple linear regression model

- Estimate the model (equation)

- Calculate the multiple determination coefficient and interpret 

- Calculate standard deviation and interpret

- Is the regression model significant (overall) ?

- Interpret each coefficient of each explanatory variable

- Evaluate the marginal contribution of each explanatory variable by using a treshold of 5%

- Create a confidence interval of 95% for each coefficient of each explanatory variable

- Analyze the colinearity between the explanatory variables

- Determine the Durbin Watson statistic and test the hypothesis of absence (or not) of auto-correlation with a treshold of 5%

- Test the normality of the random errors with Shapiro-Wilk





Author : Marion Estoup 

E-mail : marion_110@hotmail.fr

February 2023
